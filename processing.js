'use strict';

const {Op} = require("sequelize");
const {Request} = require('./models/request');
const config = require("./config/config");
const axios = require("axios");

async function processing() {
    console.log("start processing");
    let request = await Request.findOne({
        where: {
            status_code: "processing",
            uuid: null
        }
    });
    if (request) {
        let postBody = {
            request: {
                lastname: request.lastName,
                firstname: request.firstName,
                middlename: request.middleName,
                phone: request.phone,
                email: request.email
            }
        };
        console.log({postBody});
        let banksResponse = await axios.post("http://localhost:5000/request", postBody);
        console.log({banksResponse});
        await request.update({uuid: banksResponse.data.request.id});
        return
    }
    request = await Request.findOne({
        where: {
            status_code: "processing",
            uuid: {
                [Op.not]: null
            }
        }
    });
    if (!request) {
        return;
    }
    let banksResponse = await axios.get("http://localhost:5000/request/" + request.uuid);
    if (banksResponse.data.request.status_code !== "processing") {
        await request.update({status_code: banksResponse.data.request.status_code});
    }
}

setInterval(() => {
    processing().then(console.log).catch(console.error)
}, config.processingInterval);