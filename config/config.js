'use strict';

const config = {
    "development": {
        "username": process.env.DBUSERNAME || "postgres",
        "password": process.env.DBPASSWORD || "password",
        "database": process.env.DBNAME || "postgres",
        "host": process.env.DBHOST || "127.0.0.1",
        "port": process.env.DBPORT || 5432,
        "dialect": "postgres"
    },
    "test": {
        "username": process.env.DBUSERNAME || "postgres",
        "password": process.env.DBPASSWORD || "password",
        "database": process.env.DBNAME || "postgres",
        "host": process.env.DBHOST || "127.0.0.1",
        "port": process.env.DBPORT || 5432,
        "dialect": "postgres"
    },
    "production": {
        "username": process.env.DBUSERNAME || "postgres",
        "password": process.env.DBPASSWORD || "password",
        "database": process.env.DBNAME || "postgres",
        "host": process.env.DBHOST || "127.0.0.1",
        "port": process.env.DBPORT || 5432,
        "dialect": "postgres"
    },
    processingInterval: process.env.PROCESSING_INTERVAL || 1000
};

module.exports = config;