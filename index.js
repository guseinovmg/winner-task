"use strict";

const Koa = require('koa');
const koaBody = require('koa-body');
const app = module.exports = new Koa();
require("./processing");

app.use(koaBody({
    jsonLimit: '10kb'
}));

app.use(require('./routes/requests.js').routes());

if (!module.parent) app.listen(3000);