'use strict';

const {sequelize, Sequelize} = require("../db");
const DataTypes = Sequelize.DataTypes;

class Request extends Sequelize.Model {
}

Request.init({
    id: {
        type: DataTypes.BIGINT,
        primaryKey: true,
        autoIncrement: true
    },
    uuid: {
        type: DataTypes.UUID,
        allowNull: true
    },
    lastName: {
        type: DataTypes.STRING,
        allowNull: false
    },
    firstName: {
        type: DataTypes.STRING,
        allowNull: false
    },
    middleName: {
        type: DataTypes.STRING,
        allowNull: false
    },
    phone: {
        type: DataTypes.STRING,
        allowNull: false
    },
    email: {
        type: DataTypes.STRING,
        allowNull: false
    },
    status_code: {
        type: DataTypes.STRING,
        defaultValue: "processing",
        validate: {
            isIn: [["processing", "approved", "rejected", "error"]]
        }
    },
}, {
    underscored: true,
    sequelize,
    modelName: 'requests',
    timestamps: false
});

module.exports = {Request};