"use strict";

const Koa = require('koa');
const koaBody = require('koa-body');
const router = require('koa-router')();
const app = module.exports = new Koa();
const uuidv1 = require('uuid/v1');
const validator = require("validator");
const {Request} = require("./models/request");

app.use(koaBody({
    jsonLimit: '10kb'
}));

let creditRequests = [];
Request.findAll().then(result => {
    console.log(result.dataValues);
    if (Array.isArray(result.dataValues)) {
        creditRequests = result.dataValues;
        return;
    }
    if (result.dataValues instanceof Object) {
        creditRequests = [result.dataValues]
    }
});

router.post('/request', async (ctx, next) => {
    try {
        ctx.status = 201;
        let request = ctx.request.body.request;
        if (!request) {
            throw new Error("bad request")
        }
        let {lastname, firstname, middlename, phone, email} = request;
        if (validator.isEmpty(lastname)) {
            throw new Error("bad request: empty lastname");
        }
        if (validator.isEmpty(firstname)) {
            throw new Error("bad request: empty firstname");
        }
        if (validator.isEmpty(middlename)) {
            throw new Error("bad request: empty middlename");
        }
        if (!validator.isEmail(email)) {
            throw new Error("bad request: invalid email");
        }
        if (!validator.isMobilePhone(phone)) {
            throw new Error("bad request: invalid phone");
        }
        let newRequest = {
            lastname,
            firstname,
            middlename,
            phone,
            email,
            status_code: "processing",
            uuid: uuidv1()
        };
        creditRequests.push(newRequest);
        ctx.body = {
            request: {
                id: newRequest.uuid,
                status_code: newRequest.status_code
            }
        };
    } catch (err) {
        ctx.throw(400, err);
    }
});

router.get('/request/:id', async (ctx, next) => {
    try {
        let id = ctx.params.id;
        let request = creditRequests.find(e => e.uuid === id);
        if (!request) {
            ctx.status = 404;
            ctx.body = "Not found";
            return
        }
        ctx.body = {
            request: {
                id: request.uuid,
                status_code: request.status_code
            }
        };
    } catch (err) {
        ctx.throw(400, err);
    }
});

app.use(router.routes());

if (!module.parent) app.listen(5000);

setInterval(() => {
    for (let r of creditRequests) {
        if (r.status_code === "processing") {
            r.status_code = "approved"
        }
    }
}, 3000);