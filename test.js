const app = require('./index');
const server = app.listen();
const request = require('supertest').agent(server);

describe('Test', function () {
    after(function () {
        server.close();
    });

    describe('POST /request', function () {
        describe('valid input', function () {
            it('should 201', function (done) {
                request
                    .post('/request')
                    .send({
                        request: {
                            lastname: "Guseinov",
                            firstname: "Murad",
                            middlename: "G",
                            phone: "+79297777777",
                            email: "guseinovmg@gmail.com"
                        }
                    }).expect(201, done);
            });
        });

        describe('invalid input', function () {
            it('invalid structure', function (done) {
                request
                    .post('/request')
                    .send({
                        lastname: "Guseinov",
                        firstname: "Murad",
                        middlename: "G",
                        phone: "+79297777777",
                        email: "guseinovmg@gmail.com"
                    })
                    .expect(400, done);
            });
            it('invalid phone', function (done) {
                request
                    .post('/request')
                    .send({
                        request: {
                            lastname: "Guseinov",
                            firstname: "Murad",
                            middlename: "G",
                            phone: "aaa97777777",
                            email: "guseinovmg@gmail.com"
                        }
                    })
                    .expect(400, done);
            });
            it('invalid email', function (done) {
                request
                    .post('/request')
                    .send({
                        request: {
                            lastname: "Guseinov",
                            firstname: "Murad",
                            middlename: "G",
                            phone: "+79297777777",
                            email: "guseinovmg%gmail.com"
                        }
                    })
                    .expect(400, done);
            });
        });
    });

    describe('GET /request', function () {
        describe('valid input', function () {
            it('should work', function (done) {
                request
                    .get('/request')
                    .expect(200, done);
            });
        });
    });
});