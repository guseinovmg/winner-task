'use strict';

const {Request} = require('../models/request');
const validator = require("validator");

async function create(requestBody) {
    let request = requestBody.request;
    if (!request) {
        throw new Error("bad request")
    }
    let {lastname: lastName, firstname: firstName, middlename: middleName, phone, email} = request;
    if (validator.isEmpty(lastName)) {
        throw new Error("bad request: empty lastname");
    }
    if (validator.isEmpty(firstName)) {
        throw new Error("bad request: empty firstname");
    }
    if (validator.isEmpty(middleName)) {
        throw new Error("bad request: empty middlename");
    }
    if (!validator.isEmail(email)) {
        throw new Error("bad request: invalid email");
    }
    if (!validator.isMobilePhone(phone)) {
        throw new Error("bad request: invalid phone");
    }
    let result = await Request.create({
        lastName,
        firstName,
        middleName,
        phone,
        email
    });
    return {
        request: result
    }
}

async function get() {
    return Request.findAll()
}

module.exports = {get, create};