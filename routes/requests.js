'use strict';

const router = require('koa-router')();
const requestsController = require('../controllers/requests');

router.post('/request', async (ctx, next) => {
    try {
        ctx.status = 201;
        ctx.body = await requestsController.create(ctx.request.body);
    } catch (err) {
        ctx.throw(400, err);
    }
});

router.get('/request', async (ctx, next) => {
    try {
        ctx.body = await requestsController.get();
    } catch (err) {
        ctx.throw(400, err);
    }
});

module.exports = router;