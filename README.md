# README #

### Установка

Установить переменные окружения (см. config/config.js)

`cd winner-task`

`npm install`

`node install.js`

`sudo npm install nodemon mocha -g`  (для запуска тестов)


### Запуск

`nodemon index.js` 

`nodemon bank-api.js` 

Проверяем в браузере http://localhost:3000/request


### Тест

`mocha test.js`


